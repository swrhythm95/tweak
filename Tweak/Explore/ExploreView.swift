//
//  ExploreView.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct ExploreView: View {
	@State private var searchText:String = ""
	@State private var showFilter:Bool = false
	@State private var applyFilter:Bool = false
	@State private var serverCaptionData = [ServerCaptionData]()
	@State var showBlankstate:Bool = false
	@FetchRequest(entity: BlockedAccounts.entity(), sortDescriptors: []) var blockedAccounts: FetchedResults<BlockedAccounts>
	//Filter
	@State var businessCategory = UserDefaults.standard.string(forKey: "businessCategory") == nil ? "" : UserDefaults.standard.string(forKey: "businessCategory")!
	@State var archetype = ""
	@State var objective = ""
	@State var sorter = "Relevancy"
	@State var page = 1
	@State var captionShown = 0
	@Binding var selectedTab:Int
	var body: some View {
		//MARK: POINT 1
		NavigationView {
			ZStack{
				if applyFilter {
					Color("GrayBg")
						.onAppear{
							loadData()
							applyFilter.toggle()
						}
				}
				
				Color("GrayBG")
					.edgesIgnoringSafeArea(.all)
				
					VStack {
						Spacer()
							.frame(height: 50)
						
						ScrollView {
							
							ForEach(serverCaptionData.filter { caption in
								filterBlockedAccount(account: caption.username)
							}, id: \.id){ item in
								if item.caption.lowercased().contains(searchText.lowercased()) || searchText == ""{
									
									ExploreCard(profpicURL: item.profpicURL, username: item.username, captionBody: item.caption, id: item.id,selectedTab:$selectedTab)
										.background(Color.white)
										.cornerRadius(17)
										.shadow(color: Color.black.opacity(0.1), radius: 10, x:2, y:2)
										.padding([.top, .horizontal])
										.onAppear{
											captionShown += 1
										}
										.onDisappear{
											captionShown -= 1
										}
								}
							}
							if captionShown == 0{
								VStack{
									Spacer()
									HStack {
										Spacer()
										Image("TweakSearchNotFound")
										Spacer()
									}
									HStack {
										Text("Quack, caption related to \"\(searchText)\" can't be found.")
											.font(.callout)
											.fontWeight(.semibold)
											.foregroundColor(.gray)
											.padding(.top)
									}
									HStack {
										Text("Try using different search terms or adjust the filters.")
											.font(.callout)
											.foregroundColor(.gray)
									}
									.padding(.top, 2)
									Spacer()
								}
							}
						}
						.onAppear(perform: loadData)
						.shadow(color: Color.black.opacity(0.1),radius: 10, x:2, y:2)
					}
					.padding(.top)
					
				//MARK: Searchbar
				VStack{
					VStack(spacing:0){
						HStack{
							SearchBar(text: $searchText)
							
							ZStack(alignment: .topTrailing) {
								Button(action: {
									self.showFilter.toggle()
								}) {
									Image(systemName: "slider.horizontal.3")
										.resizable()
										.frame(width: 20, height: 20)
										.padding(.trailing, 12)
										.padding(.leading)
										.foregroundColor(.black)
								}.sheet(isPresented: $showFilter) {
									ExplorerFilterView(businessCategory: $businessCategory, archetype: $archetype, objective: $objective, sorter: $sorter, applyFilter: $applyFilter)
								}
								
								//MARK: Filter active sign
								if archetype != "" || objective != "" || sorter != "Relevancy" {
									Image(systemName: "circle.fill")
										.resizable()
										.frame(width: 10, height: 10)
										.foregroundColor(Color("TweakOrange"))
										.offset(x: -8, y: -4)
								}
								
							}
						}
						.padding([.leading, .trailing, .top])
						.padding(.bottom)
						
					}
					.frame(height:50)
					.background(Color("GrayBG"))
					
					Spacer()
				}
				
			}
			.background(Color("GrayBG"))
			.navigationBarTitle("Explore")
		}
		
	}
	
	func loadData() {
		print(blockedAccounts)
		self.serverCaptionData.removeAll()
		var sorterConverted:String = ""
		let categoryConverted:String = businessCategory == "UNCATEGORIZED" ? "" : businessCategory.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "&", with: "%26")
		let archetypeConverted:String = archetype == "None" ? "" : archetype.replacingOccurrences(of: " ", with: "%20")
		let objectiveConverted:String = objective == "None" ? "" : objective.replacingOccurrences(of: " ", with: "%20")
		switch sorter {
		case "Relevancy":
			sorterConverted = "relevancy"
		case "Most Engaging":
			sorterConverted = "engaging"
		case "Top Liked":
			sorterConverted = "likeCount"
		case "Top Commented":
			sorterConverted = "commentCount"
		case "Most Popular":
			sorterConverted = "popular"
		case "Hot Topic":
			sorterConverted = "hotTopic"
		case "Most Recent":
			sorterConverted = "recent"
		case "None":
			sorterConverted = ""
		default:
			sorterConverted = ""
		}
		guard let url = URL(string: "https://tweak.omkstyakobus.org/caption/fetch?category=\(categoryConverted)&archetype=\(archetypeConverted)&objective=\(objectiveConverted)&sorter=\(sorterConverted)&page=\(page)") else {
			
			print("https://tweak.omkstyakobus.org/caption/fetch?category=\(categoryConverted)&archetype=\(archetypeConverted)&objective=\(objectiveConverted)&sorter=\(sorterConverted)&page=\(page)")
			
			print("Invalid URL")
			return
		}
		print(url)
		let request = URLRequest(url: url)
		URLSession.shared.dataTask(with: request) { data, response, error in
			if let data = data {
				if let decodedResponse = try? JSONDecoder().decode([ServerCaptionData].self, from: data) {
					DispatchQueue.main.async {
						for i in decodedResponse {
							self.serverCaptionData.append(ServerCaptionData(id: i.id, businessCategory: i.businessCategory, caption: i.caption, username: i.username, profpicURL: i.profpicURL, archetype: i.archetype, objective: i.objective, picURL: i.picURL))
						}
						//print(decodedResponse)
					}
					return
				}
			}
			print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
		}.resume()
	}
	
	func filterBlockedAccount(account: String) -> Bool {
		for item in blockedAccounts {
			if item.value(forKey: "username") as! String == account {
				return false
			}
		}
		return true
	}
}

struct ExploreView_Previews: PreviewProvider {
	static var previews: some View {
		ExploreView(selectedTab: .constant(1))
	}
}
