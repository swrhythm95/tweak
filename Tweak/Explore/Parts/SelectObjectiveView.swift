//
//  SelectObjectiveView.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct SelectObjectiveView: View {
	@Environment(\.presentationMode) var presentationMode
	@Binding var objective:String
	@State var listObjective:[String] = []
	@State var showObjectiveHelp:Bool = false
	var body: some View {
		ZStack{
			Color("GrayBG")
				.edgesIgnoringSafeArea(.all)
			
			
			VStack {
				ZStack{
					HStack{
						Spacer()
						Text("Objective")
							.fontWeight(.semibold)
						Spacer()
					}
					.padding([.horizontal,.top,.bottom])
					.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
					HStack{
						Button(action: {
							self.presentationMode.wrappedValue.dismiss()
						}) {
							Text("Cancel")
								.font(.body)
								.foregroundColor(Color("TweakOrange"))
						}
						Spacer()
						Button(action: {
							self.showObjectiveHelp.toggle()
						}) {
							Image(systemName: "questionmark.circle")
								.padding(3)
						}
					}.padding([.horizontal,.top,.bottom])
					.sheet(isPresented: $showObjectiveHelp) {
						ObjectiveHelp(showObjectiveHelp: $showObjectiveHelp)
					}
				}
				
				
				Divider()
					.offset(y:-8)
				
				VStack{
					VStack{
						Button(action: {
							objective = "None"
							self.presentationMode.wrappedValue.dismiss()
						}) {
							HStack {
								Text("All")
									.foregroundColor(.black)
									.padding(.bottom,5)
								Spacer()
							}
						}
						Divider()
						ForEach(listObjective,id: \.self){item in
							Button(action: {
								objective = item
								self.presentationMode.wrappedValue.dismiss()
							}) {
								HStack {
									Text(item.capitalized)
										.foregroundColor(.black)
										.padding(.bottom,5)
									Spacer()
								}
							}
							if item != listObjective.last {
								Divider()
							}
						}
					}
					.padding()
					.background(Color.white)
					.cornerRadius(15)
					.onAppear{loadObjective()}
					Spacer()
				}
				.padding()
			}
		}
	}
	func loadObjective() {
		self.listObjective.removeAll()
		guard let url = URL(string: "https://tweak.omkstyakobus.org/objectivelist") else {
			print("Invalid URL")
			return
		}
		print(url)
		let request = URLRequest(url: url)
		
		URLSession.shared.dataTask(with: request) { data, response, error in
			if let data = data {
				if let decodedResponse = try? JSONDecoder().decode([ObjectiveList].self, from: data) {
					DispatchQueue.main.async {
						for i in decodedResponse {
							self.listObjective.append(i.objective)
						}
					}
					return
				}
			}
			print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
		}.resume()
	}
}

struct SelectObjectiveView_Previews: PreviewProvider {
    static var previews: some View {
		SelectObjectiveView(objective: .constant("Lorem"))
    }
}
