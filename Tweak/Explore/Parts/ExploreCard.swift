//
//  ExploreCard.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI
import PartialSheet

struct ExploreCard: View {
	var profpicURL:String
	var username:String
	var captionBody:String
	var id:Int
	@State var reportReason:String = ""
	@State var showActionSheet: Bool = false
	@EnvironmentObject var partialSheetManager: PartialSheetManager
	@Environment(\.managedObjectContext) var moc
	@Binding var selectedTab:Int
	@State var showPharaphase:Bool = false
    var body: some View {
		VStack(alignment: .leading){
			HStack{
				ImageViewWidget(imageUrl: profpicURL)
					.padding([.top, .leading])
				Text(username)
					.fontWeight(.semibold)
					.padding([.top])
				Spacer()
				Image(systemName: "ellipsis")
					.padding([.top, .trailing])
					.onTapGesture {
						self.showActionSheet = true
					}
					.actionSheet(isPresented: $showActionSheet) {
						ActionSheet(title: Text("Report inappropriate contents. Block accounts that you don't want to see."), buttons: [
							.destructive(Text("Report")) {
								print("Report")
								self.partialSheetManager.showPartialSheet({
								}) {
									VStack{
										TextField("Report reason", text: $reportReason)
											.padding()
										Button("Send Report", action: {
											reportCaption()
											self.partialSheetManager.closePartialSheet()
										})
										.padding()
									}
								}
							},
							.default(Text("Block \(self.username)")) {
								print("Block")
								blockAccount(account: self.username)
							},
							.cancel()
						])
					}
			}
			Text(captionBody)
				.padding([.leading, .trailing])
			Divider()
				.padding([.leading, .trailing])
			HStack{
				Spacer()
				Button(action: {
					showPharaphase.toggle()
				}) {
					HStack{
						Spacer()
						Image(systemName: "square.and.pencil")
							.foregroundColor(Color.black)
						Spacer()
					}
				}
				.padding(15)
				.fullScreenCover(isPresented: $showPharaphase, content: {
					ParaphraseView(profpic: profpicURL, username: username, caption: captionBody,showPharaphase:$showPharaphase,selectedTab:$selectedTab)
				})
				
				Spacer()
				Spacer()
				Button(action: {
					//FIXME: Bookmark
					print("Bookmark")
				}) {
					HStack{
						Spacer()
						Image(systemName: "bookmark")
							.foregroundColor(Color.black)
						Spacer()
					}
				}
				.padding(15)
				//.foregroundColor(!isBookmarked() ? Color.black : Color.init(red: 234/245, green: 116/245, blue: 0/245))
				Spacer()
			}
		}
		
    }
	func reportCaption() {
		let originalString = reportReason
		let reason = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
		let id = self.id
		if let url = URL(string: "https://tweak.omkstyakobus.org/report/\(reason ?? "error")/\(id)") {
			URLSession.shared.dataTask(with: url) { data, response, error in
				if let data = data {
					DispatchQueue.main.async {
						if String(decoding: data, as: UTF8.self) == "1" {
							let alert = UIAlertController(title: "Report", message: "Report Success", preferredStyle: .alert)
							alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
						}else{
							let alert = UIAlertController(title: "Report", message: "Report Failed", preferredStyle: .alert)
							alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
						}
					}
				}
			}.resume()
		}
	}
	
	func blockAccount(account: String) {
		print("---\(account)")
		let blockedAccounts = BlockedAccounts(context: moc)
		blockedAccounts.username = account
		try? moc.save()
	}
}

struct ExploreCard_Previews: PreviewProvider {
    static var previews: some View {
		ExploreCard(profpicURL: "a", username: "Lorem", captionBody: "Lorem", id: 1, selectedTab: .constant(1))
    }
}

