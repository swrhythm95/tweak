//
//  ObjectiveHelp.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct ObjectiveHelp: View {
	@Environment(\.presentationMode) var presentationMode
	@Binding var showObjectiveHelp:Bool
	var objectives = ["Brand Awareness", "Promotion and Sales", "Product Launching", "Cause Campaign", "Instagram Contest"]
	var objectivesDescription = ["During an awareness campaign, your main objective is to increase the visibility of your company, product, or service. This campaign is best to showcase what is distinct, exciting, and exceptional about your brand.", "This marketing campaign rewards engaged followers and encourages them to visit your account for sales and discount codes. Certain holidays and times of year are perfect for sale campaigns.", "Build anticipation and excitement around your product launch. This campaign gives you an opportunity to show off the different advantages and qualities of your new product.", "Younger consumers care not just about what you sell, but also about the ethics and social responsibility of your brand. A cause campaign is an effective way build trust & loyalty from an audience that shares your values.", "Contests are big on instagram as they are effective at drive engagement. Approaching newer audiences & growing your mailing list are also benefits you can get from running a contest."]
	var body: some View {
		ZStack{
			Color("GrayBG")
				.edgesIgnoringSafeArea(.all)
			
			
			VStack {
				ZStack{
					HStack{
						Spacer()
						Text("What is Objective")
							.fontWeight(.semibold)
						Spacer()
					}
					.padding([.horizontal,.top,.bottom])
					.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
					HStack{
						Spacer()
						Button(action: {
							self.presentationMode.wrappedValue.dismiss()
						}) {
							Text("Done")
								.fontWeight(.semibold)
						}
					}.padding([.horizontal,.top,.bottom])
				}
				
				Divider()
					.offset(y:-8)
				
				VStack{
					Text("Tweak’s objective filtering function enables you to easily find marketing campaign inspirations for your business. Below are a brief overview of what each marketing campaign does & how your brand can benefit from it.")
						.font(.footnote)
						.foregroundColor(.secondary)
						.padding(.bottom)
					ScrollView(showsIndicators: true) {
						VStack(alignment: .leading, spacing:0){
							ForEach(0 ..< objectives.count) {
								Text(objectives[$0])
									.font(.callout)
									.padding(.bottom, 4)
								Text(objectivesDescription[$0])
									.font(.footnote)
									.foregroundColor(.secondary)
									.padding(.bottom, 24)
							}
						}
					}
				}
				.padding(.horizontal)
			}
		}
	}
}

struct ObjectiveHelp_Previews: PreviewProvider {
	static var previews: some View {
		ObjectiveHelp(showObjectiveHelp: .constant(true))
	}
}
