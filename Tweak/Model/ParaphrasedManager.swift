//
//  ParaphrasedManager.swift
//  Tweak
//
//  Created by Steven Wijaya on 25/11/20.
//

import Foundation
import Combine

class ParaphasedCaptions : ObservableObject {
	let objectWillChange = ObservableObjectPublisher()
	@Published var paraphasedCaptions : [String] = []{
		willSet {
			self.objectWillChange.send()
		}
	}
	@Published var ready : Bool = false{
		willSet {
			self.objectWillChange.send()
		}
	}
	
	var apiQuotaAvailable = "-1"
	func reset() {
		ready = false
	}
	
	func paraphaseStart(inputCaption: String, qty: Int){
		if ready == false{
			var request = URLRequest(url: URL(string: "https://tweak.omkstyakobus.org/api/askPermission")!)
			request.httpMethod = "GET"
			
			let session = URLSession.shared
			let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
				if (error != nil) {
					print(error as Any)
				} else {
					_ = response as? HTTPURLResponse
					self.apiQuotaAvailable = String(decoding: data!, as: UTF8.self)
					
					if self.apiQuotaAvailable == "1"{
						var source = ""
						if self.paraphasedCaptions.count == 0 {
							source = inputCaption
						}else{
							source = self.paraphasedCaptions.last!
						}
						
						let headers = [
							"x-rapidapi-host": "paraphrasing-tool1.p.rapidapi.com",
							"x-rapidapi-key": "57a509397bmsh72119f1d48cbdd2p1478d4jsn3012860b64d5",
							"content-type": "application/json",
							"accept": "application/json"
						]
						let parameters = ["sourceText": source] as [String : Any]
						
						let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
						
						let request2 = NSMutableURLRequest(url: NSURL(string: "https://paraphrasing-tool1.p.rapidapi.com/api/rewrite")! as URL,
														   cachePolicy: .useProtocolCachePolicy,
														   timeoutInterval: 10.0)
						request2.httpMethod = "POST"
						request2.allHTTPHeaderFields = headers
						request2.httpBody = postData! as Data
						
						let session2 = URLSession.shared
						let dataTask = session2.dataTask(with: request2 as URLRequest, completionHandler: { (data, response, error) -> Void in
							if (error != nil) {
								print(error as Any)
							} else {
								_ = response as? HTTPURLResponse
								do {
									if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
										if self.paraphasedCaptions.contains(json["newText"]! as! String){
											DispatchQueue.main.async {
												self.ready = true
											}
										}else{
											DispatchQueue.main.async {
												self.paraphasedCaptions.append(json["newText"]! as! String)
												self.objectWillChange.send()
											}
											if qty>1 {
												DispatchQueue.main.async {
													self.paraphaseStart(inputCaption: inputCaption, qty: qty-1)
												}
											}else{
												DispatchQueue.main.async {
													self.ready = true
												}
											}
										}
									}
								} catch let error as NSError {
									print("Failed to load: \(error.localizedDescription)")
								}
							}
						})
						dataTask.resume()
					}else{
						DispatchQueue.main.async {
							self.paraphasedCaptions.append("API Limit Quota Reached")
							self.ready = true
						}
					}
				}
			})
			task.resume()
		}
	}
}

