//
//  SavedView.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct SavedView: View {
	@State private var searchText:String = ""
	@State private var showAddFolder:Bool = false
	@EnvironmentObject var partialSheetManager: PartialSheetManager
	@FetchRequest(entity: Boards.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Boards.name, ascending: true)]) var boardList: FetchedResults<Boards>
	let columns = [
		GridItem(.flexible()),
		GridItem(.flexible())
	]
	var body: some View {
		NavigationView{
			ZStack{
				Color("GrayBG")
					.edgesIgnoringSafeArea(.all)
				
				//MARK: Contents
				VStack(alignment: .leading){
					Spacer()
						.frame(height: 50)
					//FIXME: Show Boards
					
					if boardList.count != 0 {
						ScrollView{
							LazyVGrid(columns: columns, spacing: 12) {
								ForEach(boardList, id: \.self) { board in
									if(String(board.name ?? "").uppercased().contains(searchText.uppercased()) || searchText == ""){
										Button(action: {
											print("hello")
										}) {
											VStack(alignment: .leading){
												HStack{
													Image(systemName: "folder.circle.fill")
														.resizable()
														.frame(width: 30, height: 30)
														.padding([.leading, .top])
														.foregroundColor(Color("TweakOrange"))
													Spacer()
													Text("\(board.captionCount)")
														.font(.title)
														.fontWeight(.semibold)
														.padding([.trailing, .top])
														.foregroundColor(Color.black)
												}
												Text("\(board.name ?? "Naming Error")")
													.font(.subheadline)
													.padding([.leading, .bottom])
													.foregroundColor(Color.black)
											}
											.background(Color.white)
											.cornerRadius(17)
											.shadow(color: Color.black.opacity(0.1), radius: 10, x:2, y:2)
											.padding(.top)
											.padding(.horizontal,7)
										}
									}
									
								}
							}
						}
					}else{
						VStack(alignment: .center){
							Image("TweakEmpty")
							
							Text("You don't have any saved captions yet")
								.font(.callout)
								.fontWeight(.semibold)
								.foregroundColor(.secondary)
								.padding(.top)
							HStack{
								Text("Tap on the")
									.font(.callout)
									.fontWeight(.regular)
									.foregroundColor(.secondary)
								Image(systemName: "folder.badge.plus")
									.foregroundColor(.secondary)
								Text("button to add a new board")
									.font(.callout)
									.fontWeight(.regular)
									.foregroundColor(.secondary)
							}
							.padding(.top, 2)
							
						}
					}
					
					
					
					
				}
				
				//MARK: Searchbar
				VStack{
					VStack(spacing:0){
						HStack{
							SearchBar(text: $searchText)
							
							ZStack(alignment: .topTrailing) {
								Button(action: {
									self.partialSheetManager.showPartialSheet({
									}) {
										newBoard(partialSheetIsPresented: $partialSheetManager.isPresented)
										//										ExploreAddBoardSheet(partialSheetIsPresented: $partialSheetManager.isPresented, previousBoardName: $previousBoardName , isNewBoard: true, isFromExplore: false)
									}
									//self.showAddFolder.toggle()
								}) {
									Image(systemName: "folder.badge.plus")
										.resizable()
										.frame(width: 20, height: 18)
										.padding(.trailing, 12)
										.padding(.leading)
								}
								//.sheet(isPresented: $showAddFolder) {
								//TODO: Filter View
								/*
								NavigationView{
								ExploreFilterView(popBack: $showFilter)
								.environmentObject(userSettings)
								.environmentObject(captionList)
								}
								*/
								//}
							}
						}
						.padding([.leading, .trailing, .top])
						.padding(.bottom)
						
					}
					.frame(height:50)
					.background(Color("GrayBG"))
					
					Spacer()
				}
			}
			.navigationBarTitle("Saved")
		}
	}
}

struct SavedView_Previews: PreviewProvider {
	static var previews: some View {
		SavedView()
	}
}
