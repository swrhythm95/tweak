import SwiftUI
import CoreData

struct newBoard: View {
    //@FetchRequest(entity: Boards.entity(), sortDescriptors: []) var board: FetchedResults<Boards>
    //@FetchRequest(entity: Captions.entity(), sortDescriptors: []) var captionss: FetchedResults<Captions>
    //@ObservedObject var boards = Boards()
    //@Environment(\.managedObjectContext) var moc
	@Environment(\.managedObjectContext) var moc
    //@Environment(\.presentationMode) var presentationMode
    @State var boardNameText: String = ""
	@State var boardIconName: String = ""
    @Binding var partialSheetIsPresented: Bool
    //@Binding var previousBoardName: String
    //var isNewBoard: Bool
    //var isFromExplore: Bool

	var body: some View {
        VStack{
            HStack{
				Text("Save")
				.foregroundColor(.clear)
				.padding(.leading)
                Spacer()
                Text("New Folder")
                    .fontWeight(.semibold)
                Spacer()
                if boardNameText.count == 0 {
                    Text("Save")
                        .fontWeight(.semibold)
                        .foregroundColor(.secondary)
                        .padding(.trailing)
                    
                } else {
                Button(action: {
                    addBoard()
                    partialSheetIsPresented = false
                }) {
                    Text("Save")
                        .fontWeight(.semibold)
                }
                    .foregroundColor(Color("TweakOrange"))
                    .padding(.trailing)
                }
                
            }.padding(.bottom)
            
            ZStack {
                Color("GrayBG")
                    .edgesIgnoringSafeArea(.all)
            
				
				VStack {
					TextField("New Folder", text: $boardNameText)
						.padding(7)
						.padding(.horizontal, 1)
						.background(Color.init(red: 215/255, green: 215/255, blue: 215/255))
						.cornerRadius(8)
						.padding(.horizontal, 50)
						.overlay(
							HStack {
								Spacer()
								Button(action: {
									self.boardNameText = ""
								}) {
									Image(systemName: "multiply.circle.fill")
										.foregroundColor(Color.init(red: 142/255, green: 142/255, blue: 147/255))
										.padding(.trailing, 8)
								}
							}
							.padding(.horizontal, 50)
						)
						.padding(.vertical, 50)
				}
            }
            .frame(height: 110)
        }
        
    }
    
    func addBoard() {
        let name = self.boardNameText
        let board = Boards(context: self.moc)
        board.name = "\(name)"

        try? self.moc.save()
    }
//
//    func editBoard(previousBoardName2: String, boardName: String) {
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
//        let managedContex = appDelegate.persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Board")
//        fetchRequest.predicate = NSPredicate(format: "name == %@", previousBoardName2)
//        do{
//            let result = try managedContex.fetch(fetchRequest)
//            (result[0] as AnyObject).setValue(boardName, forKey: "name")
//            do{
//                try managedContex.save()
//            }catch{
//                print(error)
//            }
//        }catch{
//            print("Failed")
//        }
//
//        for item in captionss {
//
//            var board = item.value(forKey: "board") as! [String]
//            for i in 0 ..< board.count {
//                if board[i] == previousBoardName2 {
//                    board[i] = boardName
//                }
//            }
//            var board2 = board as? NSObject
//            item.board = board2
//        }
//        try? moc.save()
//
//        previousBoardName = boardName
//    }
//
//    func saveCaptionHistory(caption: String, uuid: String) {
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
//        let managedContex = appDelegate.persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TweakedCaptions")
//        fetchRequest.predicate = NSPredicate(format: "id == %@", uuid)
//        do{
//            let result = try managedContex.fetch(fetchRequest)
//            (result[0] as AnyObject).setValue(caption, forKey: "captionBody")
//            (result[0] as AnyObject).setValue(NSDate(), forKey: "lastEdited")
//            do{
//                try managedContex.save()
//            }catch{
//                print(error)
//            }
//        }catch{
//            print("Failed")
//        }
//    }
}
