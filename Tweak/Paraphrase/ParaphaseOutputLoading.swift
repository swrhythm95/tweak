import SwiftUI

struct ParaphaseOutputLoading: View {
	@State private var animate = false
	
	var body: some View {
		let gradient1: [UIColor] = [.systemGray4, .systemGray6]
		let gradient2: [UIColor] = [.systemGray6, .systemGray4]
		HStack{
			VStack(alignment: .leading){
				ScrollView{
					HStack{
						Text("  ")
						Spacer()
					}
				}
			}.padding()
			.background(Color.clear.modifier(AnimatableGradient(from: gradient1, to: gradient2, pct: animate ? 1 : 0)))
			.onAppear {
				withAnimation(Animation.easeInOut(duration: 0.8).repeatForever()) {
					self.animate.toggle()
				}
			}
			.cornerRadius(15.0)
			.frame(height: 196)
			.shadow(color: Color.black.opacity(0.1),radius: 10, x:2, y:2)
		}
		.padding(.horizontal)
	}
}
