//
//  TweakedView.swift
//  Tweak
//
//  Created by Steven Wijaya on 24/11/20.
//

import SwiftUI

struct TweakedView: View {
	static let taskDateFormat: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "dd MMMM yyyy, HH:mm"
		return formatter
		
	}()
	@State var sortDesc = "MR"
	@FetchRequest(entity: Tweaked.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Tweaked.lastEdited, ascending: false)]) var tweakedCaptions: FetchedResults<Tweaked>
	@FetchRequest(entity: Tweaked.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Tweaked.title, ascending: true)]) var tweakedCaptionsAZ: FetchedResults<Tweaked>
	@FetchRequest(entity: Tweaked.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Tweaked.title, ascending: false)]) var tweakedCaptionsZA: FetchedResults<Tweaked>
	
	@Environment(\.managedObjectContext) var moc
	
	@State var showSortModal: Bool = false
	@State var sorter:String = "Most Recent"
	@State private var searchText = ""
	@State private var showCancelButton: Bool = false
	@State private var isEditingSearchBar = false
	@State var copiedNotif:CGFloat = -UIScreen.screenHeight
	@State var copiedNotifOpacity = 0.0
	@State private var showDeleteConfirmation = false
	@State var captionIDToDelete = ""
	
	//@EnvironmentObject var appState: AppState
	@State private var showFilter:Bool = false
	
	var body: some View {
		ZStack {
			NavigationView{
				ZStack{
					Color("GrayBG")
						.edgesIgnoringSafeArea(.all)
					
					//MARK: Contents
					VStack(alignment: .leading){
						Spacer()
							.frame(height: 50)
						
						ScrollView {
							if sortDesc == "MR"{
								ForEach(tweakedCaptions, id: \.self) { tweakedCaption in
									if(tweakedCaption.title!.uppercased().contains(searchText.uppercased()) || searchText == ""){
										VStack(alignment: .leading,spacing:10){
											Text(tweakedCaption.title ?? "-")
												.fontWeight(.semibold)
											NavigationLink(destination: editCaptionView(caption: "\(tweakedCaption.tweakedBody!)", uuid: "\(tweakedCaption.id!)")) {
												Text(tweakedCaption.tweakedBody ?? "-")
													.fixedSize(horizontal: false, vertical: true)
													.foregroundColor(.black)
											}
											Text("Last modified on \(tweakedCaption.lastEdited!,formatter: Self.taskDateFormat)")
												.font(.system(size: 12))
												.foregroundColor(.gray)
											Divider()
											HStack{
												Spacer()
												Button(action: {
													let pasteboard = UIPasteboard.general
													pasteboard.string = tweakedCaption.tweakedBody ?? "-"
													triggerCopiedNotif()
													let generator = UINotificationFeedbackGenerator()
													generator.notificationOccurred(.success)
												}) {
													HStack {
														Spacer()
														Image(systemName: "doc.on.doc")
															.foregroundColor(.black)
														Spacer()
													}
												}
												.padding(.vertical, 1.0)
												Spacer()
												Spacer()
												Button(action: {
													captionIDToDelete = "\( tweakedCaption.id!)"
													showDeleteConfirmation = true
												}) {
													HStack {
														Spacer()
														Image(systemName: "trash")
															.foregroundColor(.black)
														Spacer()
													}
												}
												.padding(.vertical, 1.0)
												.actionSheet(
													isPresented: $showDeleteConfirmation,
													content: {
														ActionSheet(
															title: Text("Are you sure you want to delete your caption permanently?"),
															buttons: [
																.destructive(
																	Text("Delete")
																		.foregroundColor(Color.red)
																) {
																	deleteCaption(
																		captionToDelete: captionIDToDelete
																	)
																},
																.cancel(
																	Text("Cancel")
																		.foregroundColor(Color("TweakOrange"))
																)
															]
														)
													}
												)
												Spacer()
											}
										}
										.padding()
										.background(Color.white)
										.cornerRadius(/*@START_MENU_TOKEN@*/15.0/*@END_MENU_TOKEN@*/)
										Spacer()
											.frame(height:22)
									}
								}
								.padding(.horizontal)
							}
							else if sortDesc == "AZ"{
								ForEach(tweakedCaptionsAZ, id: \.self) { tweakedCaption in
									if(tweakedCaption.title!.uppercased().contains(searchText.uppercased()) || searchText == ""){
										VStack(alignment: .leading,spacing:10){
											Text(tweakedCaption.title ?? "-")
												.fontWeight(.semibold)
											NavigationLink(destination: editCaptionView(caption: "\(tweakedCaption.tweakedBody!)", uuid: "\(tweakedCaption.id!)")) {
												Text(tweakedCaption.tweakedBody ?? "-")
													.fixedSize(horizontal: false, vertical: true)
													.foregroundColor(.black)
											}
											Text("Last modified on \(tweakedCaption.lastEdited!,formatter: Self.taskDateFormat)")
												.font(.system(size: 12))
												.foregroundColor(.gray)
											Divider()
											HStack{
												Spacer()
												Button(action: {
													let pasteboard = UIPasteboard.general
													pasteboard.string = tweakedCaption.tweakedBody ?? "-"
													triggerCopiedNotif()
													let generator = UINotificationFeedbackGenerator()
													generator.notificationOccurred(.success)
												}) {
													HStack {
														Spacer()
														Image(systemName: "doc.on.doc")
															.foregroundColor(.black)
														Spacer()
													}
												}
												.padding(.vertical, 1.0)
												Spacer()
												Spacer()
												Button(action: {
													captionIDToDelete = "\( tweakedCaption.id!)"
													showDeleteConfirmation = true
												}) {
													HStack {
														Spacer()
														Image(systemName: "trash")
															.foregroundColor(.black)
														Spacer()
													}
												}
												.padding(.vertical, 1.0)
												Spacer()
											}
										}
										.padding()
										.background(Color.white)
										.cornerRadius(/*@START_MENU_TOKEN@*/15.0/*@END_MENU_TOKEN@*/)
										Spacer()
											.frame(height:22)
									}
								}
								.padding(.horizontal)
							}
							else if sortDesc == "ZA"{
								ForEach(tweakedCaptionsZA, id: \.self) { tweakedCaption in
									if(tweakedCaption.title!.uppercased().contains(searchText.uppercased()) || searchText == ""){
										VStack(alignment: .leading,spacing:10){
											Text(tweakedCaption.title ?? "-")
												.fontWeight(.semibold)
											NavigationLink(destination: editCaptionView(caption: "\(tweakedCaption.tweakedBody!)", uuid: "\(tweakedCaption.id!)")) {
												Text(tweakedCaption.tweakedBody ?? "-")
													.fixedSize(horizontal: false, vertical: true)
													.foregroundColor(.black)
											}
											Text("Last modified on \(tweakedCaption.lastEdited!,formatter: Self.taskDateFormat)")
												.font(.system(size: 12))
												.foregroundColor(.gray)
											Divider()
											HStack{
												Spacer()
												Button(action: {
													let pasteboard = UIPasteboard.general
													pasteboard.string = tweakedCaption.tweakedBody ?? "-"
													triggerCopiedNotif()
													let generator = UINotificationFeedbackGenerator()
													generator.notificationOccurred(.success)
												}) {
													HStack {
														Spacer()
														Image(systemName: "doc.on.doc")
															.foregroundColor(.black)
														Spacer()
													}
												}
												.padding(.vertical, 1.0)
												Spacer()
												Spacer()
												Button(action: {
													captionIDToDelete = "\( tweakedCaption.id!)"
													showDeleteConfirmation = true
												}) {
													HStack {
														Spacer()
														Image(systemName: "trash")
															.foregroundColor(.black)
														Spacer()
													}
												}
												.padding(.vertical, 1.0)
												Spacer()
											}
										}
										.padding()
										.background(Color.white)
										.cornerRadius(/*@START_MENU_TOKEN@*/15.0/*@END_MENU_TOKEN@*/)
										Spacer()
											.frame(height:22)
									}
								}
								.padding(.horizontal)
							}
							
							
						}.shadow(color: Color.black.opacity(0.1),radius: 10, x:2, y:2)
					}
					
					//MARK: Searchbar
					VStack{
						VStack(spacing:0){
							HStack{
								SearchBar(text: $searchText)
								
								ZStack(alignment: .topTrailing) {
									Button(action: {
										self.showFilter.toggle()
									}) {
										Image(systemName: "line.horizontal.3.decrease.circle")
											.resizable()
											.frame(width: 20, height: 20)
											.padding(.trailing, 12)
											.padding(.leading)
											.foregroundColor(.black)
									}.sheet(isPresented: $showFilter) {
										//TODO: Sort View
										filtering(sorter: $sorter, sortDesc: $sortDesc)
									}
								}
							}
							.padding([.leading, .trailing, .top])
							.padding(.bottom)
							
						}
						.frame(height:50)
						.background(Color("GrayBG"))
						
						Spacer()
					}
					
					if tweakedCaptions.count == 0{
						VStack{
							Spacer()
							Image("TweakEmpty")
							Text("You don’t have any tweaked captions yet.")
								.font(.callout)
								.fontWeight(.semibold)
								.foregroundColor(.gray)
								.padding(.top)
							HStack {
								Text("Tap on the")
									.font(.callout)
									.foregroundColor(.gray)
								Image(systemName: "square.and.pencil")
									.font(.callout)
									.foregroundColor(.gray)
								Text("button to tweak captions!")
									.font(.callout)
									.foregroundColor(.gray)
							}
							.padding(.top, 2)
							Spacer()
						}
					}
				}
				.navigationBarTitle("Tweaked")
			}
			VStack {
				Spacer()
					.frame(height:10)
				copiedToClipboard()
				Spacer()
			}
			.opacity(copiedNotifOpacity)
			.animation(.easeInOut)
		}
	}
	
	func deleteCaption(captionToDelete: String) {
		for item in tweakedCaptions {
			if "\(item.value(forKey: "id")!)" == captionToDelete {
				let deleteThis = item
				moc.delete(deleteThis)
				try? moc.save()
				break
			}
		}
	}
	
	func triggerCopiedNotif(){
		DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
			copiedNotifOpacity = 1.0
		}
		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
			copiedNotifOpacity = 0.0
		}
	}
}

struct TweakedView_Previews: PreviewProvider {
    static var previews: some View {
        TweakedView()
    }
}
