import SwiftUI

struct filtering: View {
	@Environment(\.presentationMode) var presentationMode
	@Binding var sorter:String
	@Binding var sortDesc:String
	var body: some View {
		ZStack{
			Color("GrayBG")
				.edgesIgnoringSafeArea(.all)
			VStack {
				HStack{
					Button(action: {
						self.presentationMode.wrappedValue.dismiss()
					}) {
						HStack(spacing: 0){
							Image(systemName: "chevron.left")
								.font(Font.body.weight(.bold))
								.foregroundColor(Color("TweakOrange"))
							
							Text(" Tweaked")
								.font(.body)
								.foregroundColor(Color("TweakOrange"))
						}
					}
					Spacer()
						.onTapGesture {
							presentationMode.wrappedValue.dismiss()
						}
					Text("Sort by ")
						.bold()
						.onTapGesture {
							presentationMode.wrappedValue.dismiss()
						}
					Spacer()
						.onTapGesture {
							presentationMode.wrappedValue.dismiss()
						}
					Button(action: {
					}) {
						HStack(spacing: 0){
							Image(systemName: "chevron.left").foregroundColor(.white)
							Text(" Tweaked")
								.foregroundColor(.white)
						}
					}
					
				}.padding([.horizontal,.top,.bottom])
				.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
				
				Divider()
					.offset(y:-8)
				
				VStack{
					VStack{
						
						Button(action: {
							sorter = "Most Recent"
							sortDesc = "MR"
							self.presentationMode.wrappedValue.dismiss()
						}) {
							HStack {
								Text("Most Recent")
									.foregroundColor(.black)
									.padding(.bottom,5)
								Spacer()
							}
						}
						Divider()
						
						Button(action: {
							sorter = "Title A-Z"
							sortDesc = "AZ"
							self.presentationMode.wrappedValue.dismiss()
						}) {
							HStack {
								Text("Title A-Z")
									.foregroundColor(.black)
									.padding(.vertical,5)
								Spacer()
							}
						}
						Divider()
						
						Button(action: {
							sorter = "Title Z-A"
							sortDesc = "ZA"
							self.presentationMode.wrappedValue.dismiss()
						}) {
							HStack {
								Text("Title Z-A")
									.foregroundColor(.black)
									.padding(.top,5)
								Spacer()
							}
						}
					}
					.padding()
					.background(Color.white)
					.cornerRadius(15)
					Spacer()
				}
				.padding()
				.navigationBarTitle("Sort by", displayMode: .inline)
				.navigationBarBackButtonHidden(true)
				.navigationBarItems(
					leading:
						Button(action: {
							self.presentationMode.wrappedValue.dismiss()
						}) {
							HStack(spacing: 0){
								Image(systemName: "chevron.left")
								Text(" Tweaked")
								Spacer()
							}
						}
				)
			}
			.edgesIgnoringSafeArea(.all)
		}
	}
}
