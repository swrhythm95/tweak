import SwiftUI
import CoreData

struct ModifyCaptionView: View {
	@Environment(\.presentationMode) var presentationMode
	@Environment(\.managedObjectContext) var moc
	@State var caption:String
	@State var title:String
	@State var titleAlert:Bool = false
	@Binding var showPharaphase:Bool
	@Binding var showModify:Bool
	@Binding var selectedTab:Int
	var body: some View {
		ZStack{
			VStack {
				Spacer()
					.frame(height:54)
				Color("GrayBG")
					.edgesIgnoringSafeArea(.all)
			}
			
			VStack(spacing:0) {
				ZStack{
					HStack{
						Spacer()
						Text("Modify Caption")
							.fontWeight(.semibold)
						Spacer()
					}
					.padding([.horizontal,.top,.bottom])
					.background(Color(red: 248, green: 248, blue: 248, opacity: 1))
					HStack{
						Button(action: {
							showModify = false
						}) {
							HStack {
								Image(systemName: "chevron.left")
									.font(Font.body.weight(.bold))
									.foregroundColor(Color("TweakOrange"))
								Text("Tweaked")
									.font(.body)
									.foregroundColor(Color("TweakOrange"))
							}
							
						}
						Spacer()
						Button(action: {
							titleAlert.toggle()
						}) {
							HStack {
								Text("Done")
									.font(.body)
									.fontWeight(.semibold)
									.foregroundColor(Color("TweakOrange"))
							}
							
						}
					}.padding([.horizontal,.top,.bottom])
					
				}
				
				
				Divider()
				TextEditor(text: $caption)
			}
			
			if titleAlert{
				Color.black
					.opacity(0.3)
			}
			
			if titleAlert{
				VStack{
					Spacer()
					HStack{
						Spacer()
						VStack{
							HStack{
								Text("Caption Title")
									.font(.system(size: 17))
									.bold()
							}
							.padding([.horizontal,.top])
							HStack{
								Text("Aim for a concise & unique title to easily find your modified captions later.")
									.font(.system(size: 13))
									.multilineTextAlignment(.center)
							}.padding(.horizontal,21)
							HStack{
								TextField("Write Title", text: $title)
									.textFieldStyle(RoundedBorderTextFieldStyle())
									.font(.system(size: 17))
									.frame(height:17)
							}
							.padding(.horizontal,21)
							.padding(.vertical)
							Divider()
								.padding(0)
							HStack{
								Button(action: {
									self.titleAlert.toggle()
								}) {
									Spacer()
									Text("Cancel")
										.foregroundColor(Color("TweakOrange"))
									Spacer()
								}
								Divider()
								Button(action: {
									saveCaptionHistory(caption: caption, title: title)
									showModify = false
									showPharaphase = false
									selectedTab = 3
								}) {
									Spacer()
									Text("Save")
										.bold()
										.foregroundColor(Color("TweakOrange"))
									Spacer()
								}
							}
							.frame(height:50)
							.offset(CGSize(width: 0, height: -8.0))
							.padding(0)
							Spacer()
						}
						.frame(height:200)
						.padding(.bottom,-15)
						.background(Color("GrayBG"))
						.cornerRadius(15.0)
						Spacer()
					}.frame(width:300)
					Spacer()
				}
			}
		}
	}
	
	func saveCaptionHistory(caption: String, title: String) {
		let date = NSDate()
		let captionHistory = Tweaked(context: moc)
		captionHistory.id = UUID()
		captionHistory.tweakedBody = "\(caption)"
		captionHistory.title = "\(title)"
		captionHistory.lastEdited = date as Date
		try? moc.save()
		
	}
}
